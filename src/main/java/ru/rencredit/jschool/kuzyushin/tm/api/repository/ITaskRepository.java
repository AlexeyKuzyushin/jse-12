package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import ru.rencredit.jschool.kuzyushin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);
}
