package ru.rencredit.jschool.kuzyushin.tm.constant;

public interface DescriptionConst {

    String HELP = " - Display terminal commands.";

    String VERSION = "Show version info.";

    String ABOUT = "Show developer info.";

    String INFO = "Show system info";

    String EXIT = "Close application";

    String ARGUMENTS = "Show program arguments";

    String COMMANDS = "Show program commands";

    String TASK_LIST = "Show task list";

    String TASK_CLEAR = "Remove all tasks";

    String TASK_CREATE = "Create new task";

    String TASK_UPDATE_BY_ID = "Update task by id";

    String TASK_UPDATE_BY_INDEX = "Update task by index";

    String TASK_VIEW_BY_ID = "Show task by id";

    String TASK_VIEW_BY_INDEX = "Show task by index";

    String TASK_VIEW_BY_NAME = "Show task by name";

    String TASK_REMOVE_BY_ID = "Remove task by id";

    String TASK_REMOVE_BY_INDEX = "Remove task by index";

    String TASK_REMOVE_BY_NAME = "Remove task by name";

    String PROJECT_LIST = "Show project list";

    String PROJECT_CLEAR = "Remove all projects";

    String PROJECT_CREATE = "Create new project";

    String PROJECT_UPDATE_BY_ID = "Update project by id";

    String PROJECT_UPDATE_BY_INDEX = "Update project by index";

    String PROJECT_VIEW_BY_ID = "Show project by id";

    String PROJECT_VIEW_BY_INDEX = "Show project by index";

    String PROJECT_VIEW_BY_NAME = "Show project by name";

    String PROJECT_REMOVE_BY_ID = "Remove project by id";

    String PROJECT_REMOVE_BY_INDEX = "Remove project by index";

    String PROJECT_REMOVE_BY_NAME = "Remove project by name";
}
