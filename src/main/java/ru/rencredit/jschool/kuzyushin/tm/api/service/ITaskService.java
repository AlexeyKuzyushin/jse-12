package ru.rencredit.jschool.kuzyushin.tm.api.service;

import ru.rencredit.jschool.kuzyushin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void add(Task task);

    void remove(Task task);

    List<Task> findALl();

    void clear();

    void create(String name);

    void create(String name, String description);

    Task findOneById(String id);

    Task findOneByName(String name);

    Task findOneByIndex(Integer index);

    Task removeOneById(String id);

    Task removeOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);
}
